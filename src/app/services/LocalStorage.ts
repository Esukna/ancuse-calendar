import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {ScheduleEvent} from '../interfaces/schedule';

@Injectable()
export class LocalStorage {
  private subject = new Subject<any>();

  /**
   * When change subject(change localStorage) return updated localStorage
   */
  public getLocalStorage(): Observable<ScheduleEvent[]> {
    return this.subject.asObservable();
  }

  /**
   * Get data of calendar from localstorage and emit new data.
   */
  public emitLocalStorage(): void {
    const ls = this.getParsedDataFromLocalStorage();
    this.subject.next(ls);
  }

  /**
   * Get from ls events and set new value, set in ls new array and emit subject
   */
  public setEventInLocalStorage(value): void {
    const ls = this.getParsedDataFromLocalStorage();
    ls.push(value);
    localStorage.setItem('calendarData', JSON.stringify(ls));
    this.subject.next(ls);
  }

  /**
   * Get from ls events and remove by transfered id, set in ls new array and emit subject
   */
  public removeEventFromLocalStorage(id): void {
    const ls = JSON.parse(localStorage.getItem('calendarData') || '[]');
    const index = ls.findIndex((item) => {
      return item.id === id;
    });
    if (index !== -1) {
      ls.splice(index, 1);
    }
    localStorage.setItem('calendarData', JSON.stringify(ls));
    this.subject.next(ls);
  }

  /**
   * Get from ls events and set new value, set in ls new array and emit subject
   */
  public editEvent(value): void {
    const ls = this.getParsedDataFromLocalStorage();
    const editedLs = ls.map((item) => {
      if (item.id === value.id) {
        item.title = value.title;
        item.description = value.description;
      }
      return item;
    });
    localStorage.setItem('calendarData', JSON.stringify(editedLs));
    this.subject.next(editedLs);
  }

  /**
   * Get parsed localstorage array
   */
  private getParsedDataFromLocalStorage(): ScheduleEvent[] {
    return JSON.parse(localStorage.getItem('calendarData') || '[]');
  }

}
