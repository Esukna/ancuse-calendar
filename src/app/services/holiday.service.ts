import { Injectable } from '@angular/core';
declare var moment: any;

@Injectable({
  providedIn: 'root'
})
export class HolidayService {

  constructor() {}

  public createHoliday(): void {
    moment.modifyHolidays.add({
      'January day': {
        date: '01/02',
        keywords_y: ['January']
      }
    })
    .add(
      {
        'February day': {
          date: '02/03',
          keywords_y: ['February']
        }
      }
    ).add(
      {
        'March day': {
          date: '03/04',
          keywords_y: ['March']
        }
      }
    ).add(
      {
        'April day': {
          date: '04/05',
          keywords_y: ['April']
        }
      }
    ).add(
      {
        'May day': {
          date: '05/06',
          keywords_y: ['May']
        }
      }
    ).add(
      {
        'June day': {
          date: '06/07',
          keywords_y: ['June']
        }
      }
    ).add(
      {
        'Jule day': {
          date: '07/08',
          keywords_y: ['Jule']
        }
      }
    ).add(
      {
        'August day': {
          date: '08/09',
          keywords_y: ['August']
        }
      }
    ).add(
      {
        'September day': {
          date: '09/10',
          keywords_y: ['September']
        }
      }
    ).add(
      {
        'October day': {
          date: '10/11',
          keywords_y: ['October']
        }
      }
    ).add(
      {
        'November day': {
          date: '11/12',
          keywords_y: ['November']
        }
      }
    ).add(
      {
        'December day': {
          date: '12/13',
          keywords_y: ['December']
        }
      }
    );
  }

}
