import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubscriptionLike} from 'rxjs/internal/types';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.sass']
})
export class EditModalComponent implements OnInit, OnDestroy {

  isVisibleModal = false;
  selectedEvent: any;
  validateFormModal: FormGroup;
  @Input() checkWordCount;
  @Output() emitEvent = new EventEmitter<any>();

  subscriptions: SubscriptionLike[] = [];

  constructor() { }

  ngOnInit() {
    this.validateFormModal = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', [this.checkWordCount.bind(this), Validators.required]),
    });
  }

  editEvent(event) {
    this.emitEvent.emit({
      editedLocalStorage: event,
      id: this.selectedEvent.id
    });
    this.handleOkModal();
  }

  showEditModal(item): void {
    this.isVisibleModal = true;
    this.selectedEvent = item;
  }

  handleOkModal(): void {
    this.isVisibleModal = false;
  }
  handleCancelkModal(): void {
    this.isVisibleModal = false;
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach( (subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }

}
