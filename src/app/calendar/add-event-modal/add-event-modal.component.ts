import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ScheduleEvent} from '../../interfaces/schedule';
import {FormControl, FormGroup, Validators} from '@angular/forms';

declare var moment: any;

@Component({
  selector: 'app-add-event-modal',
  templateUrl: './add-event-modal.component.html',
  styleUrls: ['./add-event-modal.component.sass']
})
export class AddEventModalComponent implements OnInit {

  validateForm: FormGroup;
  isVisible = false;
  time = new Date();

  @Input() selectMoment: any;
  @Input() checkWordCount: any;
  @Input() available: boolean;
  @Input() momentDate: any;
  @Output() currentLocalStorage = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
    // create validate Form
    this.validateForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', [this.checkWordCount.bind(this), Validators.required]),
      timePick: new FormControl('', [this.checkTimeAvailable.bind(this), Validators.required]),
    });
  }

  /**
   * Check if time is past or not
   */
  private checkTimeAvailable(): null | { before: boolean } {
    const isDayAfter = moment(this.momentDate, 'DD-MM-YYYY').isAfter(moment(), 'day');
    if (isDayAfter) {
      return null;
    }
    const isBefore = moment(this.time).isBefore(moment(), 'minute');
    if (isBefore) {
      return {before: true};
    } else {
      return null;
    }
  }

  /**
   * Submit function
   * take submited data and push in localstorage, clear form, and close it
   */
  submitForm() {
    const value: ScheduleEvent = {
      title: this.validateForm.controls.title.value,
      day: this.selectMoment,
      timepick: moment(this.validateForm.controls.timePick.value),
      description: this.validateForm.controls.description.value,
      id: new Date().getUTCMilliseconds() // impovization uniq id ( One in 999 case CAN NOT TO BE UNIQ !!! )
    };
    this.currentLocalStorage.emit({
      value
    });
    this.validateForm.reset();
    this.handleOk();
  }

  /**
   * Show modal
   */
  public showModal(): void {
    this.isVisible = true;
  }

  /**
   * Close modal when submit
   */
  public handleOk(): void {
    this.isVisible = false;
  }

  /**
   * Close modal when cancel modal
   */
  public handleCancel(): void {
    this.isVisible = false;
  }

}
