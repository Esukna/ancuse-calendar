import {Component, EventEmitter, Input, Output} from '@angular/core';

declare var moment: any;

@Component({
  selector: 'app-calendar-day-event',
  templateUrl: './calendar-day-event.component.html',
  styleUrls: ['./calendar-day-event.component.sass']
})
export class CalendarDayEventComponent {

  @Input() item;
  @Input() momentDate;
  @Output() emitDeleted = new EventEmitter<any>();
  @Output() emitShowModal = new EventEmitter<any>();

  constructor() {}

  /**
   * Show time created
   */
  public getTimeEventItem(item): any {
    const momentTime = moment(item.timepick).format('HH:mm');
    return momentTime;
  }

  /**
   * Delete event by click by id
   */
  public deleteEvent(id: number): void {
    this.emitDeleted.emit({
      id
    });
  }

  /**
   * Show edited modal by click edit
   */
  public showEditModal(item): void {
    this.emitShowModal.emit({
      editedItem: item
    });
  }

}
