import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Schedule, ScheduleEvent} from '../../interfaces/schedule';
import {NzMessageService} from 'ng-zorro-antd';
import {EditModalComponent} from '../edit-modal/edit-modal.component';
import {SubscriptionLike} from 'rxjs/internal/types';
import {LocalStorage} from '../../services/LocalStorage';
import {map} from 'rxjs/operators';
import * as _ from 'lodash';
import {StoreCalendar} from '../../store/store-calendar';

declare var moment: any;

@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.component.html',
  styleUrls: ['./calendar-day.component.sass']
})
export class CalendarDayComponent implements OnInit, OnDestroy {

  momentDate: string;
  num: string;
  available = false;
  celebration: any;
  @ViewChild('editModal', {static: true}) editModal: EditModalComponent;

  subscriptions: SubscriptionLike[] = [];

  scheduleData: Schedule[] = [
    {time: moment('00-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('01-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('02-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('03-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('04-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('05-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('06-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('07-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('08-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('09-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('10-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('11-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('12-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('13-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('14-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('15-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('16-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('17-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('18-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('19-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('20-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('21-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('22-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('23-00', 'hh-mm').format('HH'), event: [], availableHour: false},
  ];

  localStorage: ScheduleEvent[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private message: NzMessageService,
              private localstorageService: LocalStorage,
              private store: StoreCalendar) {
    this.subscriptions.push(
      this.store.state$.subscribe(state => {
        if (state.updated) {
          this.momentDate = state.momentDate;
          this.available = state.available;
          this.celebration = state.celebration;
        } else {
          this.router.navigate(['/']);
        }
      }, (error) => {
        console.log(error);
      })
    );
  }

  ngOnInit() {
    // set to local variable -> 'num' parameter from query ( num - number of day in month )
    this.subscriptions.push(
      this.route.params.subscribe((params) => {
        this.num = params.num;
      }, (error) => {
        console.log(error);
      })
    );
    // subscribe to localstorage
    this.getEventsFromLocalStorage();
    // put the right event on the right day
    this.setEventsInHours();
  }

  /**
   * When submit add event set in locastorage and update local variables state
   */
  submitForm(event) {
    this.localstorageService.setEventInLocalStorage(event.value);
    this.setEventsInHours();
  }

  /**
   * When click on delete, event in child component
   * return event after deleting and refresh 'scheduleData' variable
   */
  emitDeleted(event) {
    this.localstorageService.removeEventFromLocalStorage(event.id);
    this.setEventsInHours();
  }

  /**
   * When emitted edit refresh grid and local variable
   */
  editEvent(event) {
    const value = {
      title: event.editedLocalStorage.form.value.title,
      description: event.editedLocalStorage.form.value.description,
      id: event.id
    };
    this.localstorageService.editEvent(value);
    this.setEventsInHours();
  }

  /**
   * Get localstorage (subscribe) and emit changes
   */
  getEventsFromLocalStorage() {
    this.subscriptions.push(
      this.localstorageService.getLocalStorage().pipe(
        map((elem) => {
          return elem.filter((item: ScheduleEvent) => item.day === this.momentDate);
        }),
      ).subscribe((res) => {
        this.localStorage = res;
        if (res.length <= 0) {
          this.createBasicMessage();
        }
      }, (error) => {
        console.log(error);
      })
    );
    this.emitNewValueLocalStorage();
  }

  /**
   * Emit data to localstorage
   */
  emitNewValueLocalStorage() {
    this.localstorageService.emitLocalStorage();
  }

  /**
   * set inside scheduleData item new data event
   */
  setEventsInHours() {
    this.scheduleData.forEach((date) => {
      this.compareTime(date);
    });
  }

  /**
   * Compare time from 'scheduleData' and 'filteredEventShow'
   * if data equally set event in grid 'scheduleData'
   */
  compareTime(date) {
    if (this.localStorage) {
      const filteredTimeMap = this.localStorage.filter((event) => {
        const eventHour = moment(event.timepick).format('HH');
        if (date.time === eventHour) {
          return event;
        }
      });
      const sortedArray = _.orderBy(filteredTimeMap, (o: any) => {
        return moment(o.timepick).format('HH:mm');
      }, ['asc']);
      date.event = sortedArray;
    }
  }

  /**
   * Check if have min 5 word and hint how much is left
   */
  checkWordCount(control) {
    if (control.value !== null) {
      const countWord = control.value.split(' ').length;
      if (countWord < 5) {
        return {countWord: 5 - countWord};
      } else {
        return null;
      }
    }
  }

  /**
   * Show selected date in format
   */
  showSelectedDate() {
    return moment(this.momentDate, 'DD-MM-YYYY').format('MMMM DD, YYYY');
  }

  /**
   * Show name of weekend in format
   */
  showSelectedNameDate() {
    return moment(this.momentDate, 'DD-MM-YYYY').format('dddd');
  }

  /**
   * Create pop up info if havent events on selected day
   */
  createBasicMessage(): void {
    this.message.info('You have not event on this day!');
  }

  /**
   * Show modal for edit event
   */
  showEditModal(event) {
    this.editModal.showEditModal(event.editedItem);
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach((subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }
}
