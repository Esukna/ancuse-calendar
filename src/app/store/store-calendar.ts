import {Store} from 'rxjs-observable-store';
import {Injectable} from '@angular/core';
import {InitialState} from './initial-state';

@Injectable()
export class StoreCalendar extends Store<InitialState> {

  constructor() {
    super(new InitialState());
  }

  updateProperties(momentDate, available, celebration, updated): void {
    this.setState({
      ...this.state,
      momentDate,
      available,
      celebration,
      updated
    });
  }
}
