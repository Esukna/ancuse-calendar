export class InitialState {
  momentDate: string = null;
  available = false;
  celebration: string = null;
  updated = false;
}
